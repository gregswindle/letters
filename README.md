![Build Status](https://gitlab.com/pages/hexo/badges/master/build.svg)

<figure>

<a href="https://commons.wikimedia.org/wiki/File:Edge_of_the_Forest_by_George_Inness_1891.jpeg#/media/File:Edge_of_the_Forest_by_George_Inness_1891.jpeg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Edge_of_the_Forest_by_George_Inness_1891.jpeg/1200px-Edge_of_the_Forest_by_George_Inness_1891.jpeg" alt="Edge of the Forest by George Inness 1891.jpeg"></a>

<figcaption>Edge of the Forest, by <a href="https://en.wikipedia.org/wiki/en:George_Inness" class="extiw" title="w:en:George Inness"><span title="American landscape painter (1825-1894)">George Inness</span></a> - <a rel="nofollow" class="external text" href="http://ecatalogue.art.yale.edu/detail.htm?objectId=49422">Yale University Art Gallery, Yale University, New Haven, Conn.</a>, Public Domain, <a href="https://commons.wikimedia.org/w/index.php?curid=14711693"><small>(Link)</small></a>.</figcaption>

</figure>

---

<details><summary>

Example [Hexo] website using GitLab Pages.

</summary>

<br>Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation http://doc.gitlab.com/ee/pages/README.html.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: node:4.2.2

pages:
  cache:
    paths:
    - node_modules/

  script:
  - npm install hexo-cli -g
  - npm install
  - hexo deploy
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hexo and [Hexo Server][hexo-server]
1. Install dependencies: `npm install`
1. Preview your site: `hexo server`
1. Add content
1. Generate your site (optional): `hexo generate`

Read more at Hexo's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

----

Forked from @VeraKolotyuk

</details>

---

[ci]: https://about.gitlab.com/gitlab-ci/
[hexo]: https://hexo.io
[install]: https://hexo.io/docs/index.html#Installation
[documentation]: https://hexo.io/docs/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[hexo-server]: https://hexo.io/docs/server.html
